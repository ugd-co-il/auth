const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const expressSession = require('express-session')
const app = express();


function checkAuth(req, res, next) {
  console.log('checkAuth ' + req.url);
  console.log(req.session, req.session.authenticated);
  if (req.url !== '/login' && (!req.session || !req.session.authenticated)) {
    res.sendStatus(403);
    return;
  }

  next();
}

app.use(cors({credentials: true, origin: 'http://localhost:4200'}));
app.use(expressSession({ secret: 'shhhh' }));
app.use(bodyParser());

app.use(checkAuth);

app.get('/logedIn', (req, res) => {
  res.json({ok: req.session.authenticated ? true : false});
});

app.post('/login', (req, res) => {
    if(req.body.username === 'amit' && req.body.password === 'password') {
        req.session.authenticated = true;
        req.session.username = req.body.username;
        res.json({ok: true});
    } else {
      res.json({ok: false})
    }
});

app.get('/logout', (req, res) => {
  delete req.session.authenticated;
  res.sendStatus(204);
});

app.listen(8081);
