const express = require('express');
const cookieParser = require('cookie-parser');

const app = express();

app.use(cookieParser());
let i = 0;
const session = {

}

app.use((req, res, next) => {
  if(!req.cookies || !req.cookies.sid) {
    const sid = i++;

    res.cookie('sid', sid);
    session[sid] = { sid: sid };
    return res.end('sid defined');
   }

  req.session = session[req.cookies.sid];
  next();
})

app.get('/', (req, res) => {

  res.end('info: ' + JSON.stringify(req.session))
})

app.get('/generate', (req, res) => {
  req.session.word = generateWord();
  res.end('info: ' + JSON.stringify(req.session))
})

function generateWord() {
  const words = ['hi', 'hello', 'bye'];

  return words[Math.floor(Math.random() * words.length)];
}

app.listen(1337);
