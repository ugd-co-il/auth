import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {
  private username:string = '';
  private password:string = '';
  private message:string = '';

  constructor(private authService:AuthService) { }

  ngOnInit() {
  }

  loginUser() {
    this.authService.login(this.username, this.password).subscribe((answer) => {
      if(answer.ok) {
        this.message = 'ok';
      } else {
        this.message = 'not ok';
      }
    });
  }

}
