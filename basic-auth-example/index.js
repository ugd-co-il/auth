const express = require('express');
const app = express();
const basicAuthMiddleware = require('./basic-auth-middleware');

app.use(basicAuthMiddleware);

app.get('/',(req, res) => {
  res.end('ok');
});

app.listen(8081)